# Pomodoro App

### What does it do?

This project is a simple countdown timer. Simply put, is a project to get me started on react development,

### What is a Pomodoro?

The Pomodoro® Technique is a time management method developed by Francesco Cirillo in the late 1980s. It is a structured method made up of processes, tools, principles and values to learn how to deal with time and turn it from a vicious predator to an ally to boost productivity. It is based on a series of principles built on self-observation and awareness. Developing these skills makes it possible to change the relationship with time and reach our goals with less effort and anxiety.

### How to build this?

First of all, you should have node and npm installed.
Then simple run:

> npm run
