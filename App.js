import { StatusBar } from 'expo-status-bar';
import { useEffect, useState } from 'react';
import { StyleSheet, Text, View, Pressable, Alert, Vibration } from 'react-native';

export default function App() {

  var [counterText, setCounterText] = useState();
  var [buttonText, setButtonText] = useState();
  const START_SECONDS = 60 * 25;
  const VIBRATION_PATTERN = [0,150,100,150,100,150,100,150,100,150,100,150,100,150];

  useEffect(() => {
    initialize();
  }, []);

  return (
    <View style={styles.container}>
      <StatusBar style="auto" />
      <Text style={styles.title}>Pomodoro Counter</Text>
      <Text style={styles.counter}>{counterText}</Text>
      <Pressable style={styles.clickable} onPress={onStartPomodoroPress}>
        <Text style={styles.clickable_text}>Start Pomodoro</Text>
      </Pressable>
    </View>
  );

  function initialize() {
    self.secondsLeft = START_SECONDS;
    setCounterText("25:00");
    setButtonText("Start Pomodoro");
  }

  function tick() {
    self.secondsLeft = self.secondsLeft - 1;
    var min = Math.floor(self.secondsLeft / 60);
    var sec = Math.floor(self.secondsLeft % 60);
    setCounterText(`${min<9?'0'+min:min}` + ':' + `${sec<9?'0'+sec:sec}`);
    if (self.secondsLeft == 0) {
      clearInterval(self.interval);
      delete self.interval;
      setButtonText("Start Pomodoro");
      self.secondsLeft = START_SECONDS;
      Vibration.vibrate(VIBRATION_PATTERN);
      Alert.alert("Alert","Pomodoro finished!", [{"text":"OK", onPress: () => {}}])
    }
  }

  function onStartPomodoroPress() {
    if (!self.interval) {
      setButtonText("Pause Pomodoro");
      self.interval = setInterval(tick, 1000);
    } else {
      setButtonText("Start Pomodoro");
      clearInterval(self.interval);
      delete self.interval;
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  counter: {
    fontSize: 96,
    fontWeight: 'bold',
  },
  title: {
    fontSize: 32,
  },
  clickable: {
    padding: 32,
    backgroundColor: '#0071bf',
    borderRadius: 12,
  },
  clickable_text: {
    fontSize: 28,
    color: '#edf7ff',
  }
});
